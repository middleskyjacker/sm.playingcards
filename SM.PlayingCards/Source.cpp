#include <iostream>
#include <conio.h>

using namespace std;

enum Suit {
	Spade,
	Club,
	Diamond,
	Heart
};

enum Rank {
	Two = 2,
	Three = 3,
	Four = 4,
	Five = 5,
	Six = 6,
	Seven = 7,
	Eight = 8,
	Nine = 9,
	Ten = 10,
	Jack = 11,
	Queen = 12,
	King = 13,
	Ace = 14
};

struct Card {
	Suit Suit;
	Rank Rank;
};



int main() {
	Card c1;
	c1.Rank = Rank::Ten;
	c1.Suit = Suit::Spade;

	(void)_getch;
	return 0;
}